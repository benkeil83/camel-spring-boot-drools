package de.benkeil.test.drools;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Person implements Serializable
{

	private static final long serialVersionUID = 672437187658401815L;

	private Integer id;

	private String name;

	private Integer age;

	private Gender gender;

	private List<String> errors = new ArrayList<String>();


	public String getName()
	{
		return name;
	}


	public Person setName(String name)
	{
		this.name = name;
		return this;
	}


	public Integer getAge()
	{
		return age;
	}


	public Person setAge(Integer age)
	{
		this.age = age;
		return this;
	}


	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}


	public List<String> getErrors()
	{
		return errors;
	}


	public Person setErrors(List<String> errors)
	{
		this.errors = errors;
		return this;
	}


	public Integer getId()
	{
		return id;
	}


	public Person setId(Integer id)
	{
		this.id = id;
		return this;
	}


	public Gender getGender()
	{
		return gender;
	}


	public Person setGender(Gender gender)
	{
		this.gender = gender;
		return this;
	}
}
