package de.benkeil.test.drools;

import org.drools.core.common.DefaultFactHandle;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KieService
{

	private static Logger LOGGER = LoggerFactory.getLogger(KieService.class);

	@Autowired
	KieContainer kieContainer;


	@SuppressWarnings("unchecked")
	public <T> T addObjectAndFireAllRules(T object)
	{
		LOGGER.info(String.format("Add %s to kieSession", object));
		KieSession kieSession = kieContainer.newKieSession();
		DefaultFactHandle handle = (DefaultFactHandle) kieSession.insert(object);
		kieSession.getAgenda()
				.getAgendaGroup("age")
				.setFocus();
		kieSession.getAgenda()
				.getAgendaGroup("gender")
				.setFocus();
		kieSession.fireAllRules();
		kieSession.dispose();
		return (T) handle.getObject();
	}
}
