package de.benkeil.test.drools;

import java.io.IOException;

import org.apache.camel.component.servlet.CamelHttpTransportServlet;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.ReleaseId;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.io.ResourceFactory;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

@Configuration
public class ApplicationContext
{

	private final KieServices kieServices = KieServices.Factory.get();

	private static final String RULES_PATH = "rules/";


	@Bean
	public KieFileSystem kieFileSystem() throws IOException
	{
		KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
		for (Resource file : getRuleFiles())
		{
			kieFileSystem.write(ResourceFactory.newClassPathResource(RULES_PATH + file.getFilename(), "UTF-8"));
		}
		return kieFileSystem;
	}


	private Resource[] getRuleFiles() throws IOException
	{
		ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
		return resourcePatternResolver.getResources("classpath*:" + RULES_PATH + "**/*.*");
	}


	@Bean
	public KieContainer kieContainer() throws IOException
	{
		final KieRepository kieRepository = kieServices.getRepository();

		kieRepository.addKieModule(new KieModule()
		{

			public ReleaseId getReleaseId()
			{
				return kieRepository.getDefaultReleaseId();
			}
		});

		KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem());
		kieBuilder.buildAll();

		return kieServices.newKieContainer(kieRepository.getDefaultReleaseId());
	}

	// @Bean
	// public KieBase kieBase() throws IOException
	// {
	// return kieContainer().getKieBase();
	// }
	//
	//
	// @Bean
	// public KieSession kieSession() throws IOException
	// {
	// return kieContainer().newKieSession();
	// }
}
