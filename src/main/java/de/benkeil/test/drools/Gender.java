package de.benkeil.test.drools;

public enum Gender
{

	MALE,
	FEMALE;
}
