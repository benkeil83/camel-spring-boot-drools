package de.benkeil.test.drools;

import org.apache.camel.LoggingLevel;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.model.rest.RestParamType;
import org.apache.camel.spring.boot.FatJarRouter;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySpringBootRouter extends FatJarRouter
{

	@Override
	public void configure()
	{
		// @formatter:off
		restConfiguration().component("jetty")
				.host("localhost")
				.port(9080)
				.bindingMode(RestBindingMode.json);

		from("timer:trigger?period=300&repeatCount=1")
				.bean(PersonHelper.class)
				.to("log:out")
				.bean(PersonService.class)
				.choice()
					.when(simple("${body.errors.size()} > 0"))
						.log(LoggingLevel.INFO, "Sorry ${body.name}, you're not welcome. Reasons: ${body.errors}")
					.otherwise()
						.log(LoggingLevel.INFO, "Hey ${body.name}, you're welcome :)");

		rest("/person")
				.get()
					.to("direct:test")
				.get("/{id}")
					.to("direct:get-person");

		from("direct:test")
				.log(LoggingLevel.INFO, "=== TEST ===");

		from("direct:get-person")
				.log(LoggingLevel.INFO, "Get person with id ${header.id}")
				.bean(PersonService.class, "get")
				.log(LoggingLevel.INFO, "Person: ${body}")
				.to("direct:validate-person");

		from("direct:validate-person")
				.bean(PersonService.class)
				.choice()
					.when(simple("${body.errors.size()} > 0"))
						.log(LoggingLevel.INFO, "Sorry ${body.name}, you're not welcome. Reasons: ${body.errors}")
					.otherwise()
						.log(LoggingLevel.INFO, "Hey ${body.name}, you're welcome :)");

	}

}
