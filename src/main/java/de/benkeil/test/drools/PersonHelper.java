package de.benkeil.test.drools;

import java.util.concurrent.ThreadLocalRandom;

import org.apache.camel.Exchange;
import org.apache.camel.Handler;

public class PersonHelper
{

	@Handler
	public void handle(Exchange exchange)
	{
		exchange.getIn()
				.setBody(new Person().setName("Ben")
						.setAge(ThreadLocalRandom.current()
								.nextInt(18, 29)));
	}
}
