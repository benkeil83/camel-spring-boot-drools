package de.benkeil.test.drools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.apache.camel.Header;
import org.drools.core.common.DefaultFactHandle;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService
{

	private static Logger LOGGER = LoggerFactory.getLogger(PersonService.class);

	private Map<Integer, Person> persons = new HashMap<Integer, Person>()
	{

		{
			put(1, new Person().setName("Ben")
					.setAge(32)
					.setGender(Gender.MALE)
					.setId(1));
			put(2, new Person().setName("Claudia")
					.setAge(21)
					.setGender(Gender.FEMALE)
					.setId(2));
			put(3, new Person().setName("Niklas")
					.setAge(19)
					.setGender(Gender.MALE)
					.setId(3));
			put(4, new Person().setName("Guido")
					.setAge(24)
					.setGender(Gender.MALE)
					.setId(4));
			put(5, new Person().setName("Pascal")
					.setAge(30)
					.setGender(Gender.MALE)
					.setId(5));
			put(6, new Person().setName("Sarah")
					.setAge(18)
					.setGender(Gender.FEMALE)
					.setId(6));
		}
	};

	@Autowired
	KieService kieService;


	@Handler
	public void handle(@Body Person person, Exchange exchange)
	{
		exchange.getIn()
				.setBody(kieService.addObjectAndFireAllRules(person));
	}


	public Person get(@Header("id") Integer id)
	{
		return persons.get(id);
	}
}
